import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:test_app_01/service/custom/exception.dart';
import 'package:test_app_01/utilities/globals.dart';

class Services {
  static final dio = Dio();

  static Future<Response> postRequest(String apiName, Object body) async {
    String url = Globals.domainHost + apiName;
    debugPrint("url: $url");
    Response response = await dio.post(
      url,
      data: body,
      options: Options(headers: {"ContentType": "application/json; charset=UTF-8"}),
    );
    return response;
  }

  static Future<dynamic> getRequest(String apiName, Map<String, String> query) async {
    String params = '';
    if (query.isNotEmpty) {
      params = '?';
    }
    query.forEach((key, value) {
      // params = params + key + '=' + value + '&';
      params = "$params$key=$value&";
    });
    if (query.isNotEmpty) {
      params = params.substring(0, params.length - 1);
    }
    String url = Globals.domainHost + apiName + params;
    debugPrint("url: $url");

    try {
      Response response = await dio.get(
        url,
        options: Options(headers: {"ContentType": "application/json; charset=UTF-8"}),
      );
      return response.data;
    } on DioError catch (ex) {
      /// TODO: Status Code r yalgah 400, 500,
      /// TODO: Shine exception uusgeh
      ///
      if(ex.response?.statusCode == 500) {
        throw CustomException(message: 'Сервер дээр алдаа гарсан', statusCode: ex.response?.statusCode);
      }
      if(ex.response?.statusCode == 400) {
        throw CustomException(message: ex.response?.data["message"], statusCode: ex.response?.statusCode);
      }
      throw CustomException(
          message: ex.response?.data.toString() ?? "Failed", statusCode: ex.response?.statusCode);
    } catch (onError) {
      debugPrint("Error: ${onError.toString()}");
      /// App exception todorhoiloh
      rethrow;
    }
  }
}
