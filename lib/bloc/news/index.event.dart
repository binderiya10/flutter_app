import 'package:equatable/equatable.dart';
import 'package:test_app_01/model/news/models.dart';

abstract class NewsEvent extends Equatable {
  const NewsEvent();

  @override
  List<Object> get props => [];
}

class GetNews extends NewsEvent {
  final int pageId;
  final String filter;

  const GetNews({required this.filter, required this.pageId});

  @override
  List<Object> get props => [pageId, filter];
}

class AddNews extends NewsEvent {
  final NewsModelShuu news;

  const AddNews(this.news);

  @override
  List<Object> get props => [news];
}

class RemoveNews extends NewsEvent {
  final NewsModelShuu news;

  const RemoveNews(this.news);

  @override
  List<Object> get props => [news];
}
