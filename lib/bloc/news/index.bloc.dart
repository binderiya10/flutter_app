import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app_01/bloc/news/index.event.dart';
import 'package:test_app_01/bloc/news/index.state.dart';
import 'package:test_app_01/global.key.dart';
import 'package:test_app_01/model/news/models.dart';
import 'package:test_app_01/service/custom/exception.dart';
import 'package:test_app_01/service/http_service.dart';
import 'package:test_app_01/utilities/globals.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  NewsBloc() : super(NewsInitial()) {
    on<GetNews>(_mapGetNewsToState);
  }

  static NewsBloc get bloc => BlocProvider.of<NewsBloc>(GlobalKeys.currentKey.currentState!.context);

  /// [Бүх мэдээний жагсаалт авах]
  Future<FutureOr<void>> _mapGetNewsToState(GetNews event, Emitter<NewsState> emit) async {
    emit(NewsInitial());
    emit(NewsLoading());
    try {
      Map<String, String> query = {
        "page": event.pageId.toString(),
        "size": Globals.pageSize.toString(),
        "filters": event.filter != "" ? '["category_code", ${event.filter}]' : ""
      };
      var resp = await Services.getRequest(Globals.apiNews, query) as Map<String, dynamic>;
      debugPrint(resp.toString());
      debugPrint(resp['items'].toString());
      List<NewsModelShuu> list = [];
      if (resp.containsKey("items")) {
        resp['items'].forEach((item) {
          list.add(NewsModelShuu.fromJson(item));
        });
        emit(NewsSuccessful(news: list, total: resp['total'], totalPages: resp['total_pages']));
      } else {
        emit(const NewsFailure(message: 'Өгөгдөл буруу ирсэн байна'));
      }
    } on CustomException catch (error) {
      debugPrint("Error ${error.message}");
      debugPrint("Error code ${error.statusCode}");
      emit(NewsFailure(message: error.message, statusCode: error.statusCode));
    } catch (onError) {
      debugPrint("Error $onError");
      emit(NewsFailure(message: onError.toString()));
    }
  }
}
