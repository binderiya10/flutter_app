import 'package:equatable/equatable.dart';
import 'package:test_app_01/model/news/models.dart';

abstract class NewsState extends Equatable {
  const NewsState();

  @override
  List<Object> get props => [];
}

class NewsInitial extends NewsState {}

class NewsLoading extends NewsState {}

/**
 * error message `message`
 * http Status Code `statusCode`
 */
class NewsFailure extends NewsState {
  final String? message;
  final int? statusCode;

  const NewsFailure({this.message, this.statusCode});
}

class NewsSuccessful extends NewsState {
  final List<NewsModelShuu> news;
  final int total;
  final int totalPages;

  const NewsSuccessful({required this.news, required this.total, required this.totalPages});

  @override
  List<Object> get props => [news, total, totalPages];
}
