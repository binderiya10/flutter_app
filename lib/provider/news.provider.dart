import 'package:flutter/material.dart';
import 'package:test_app_01/model/news/models.dart';

class NewsProvider extends ChangeNotifier {
  final List<NewsModelShuu> _items = [];

  List<NewsModelShuu> get items => _items;

  String _animationId = '';

  String get animationId => _animationId;

  set animationId(String id){
    _animationId = id;
    notifyListeners();
  }

  set items(List<NewsModelShuu> list){
    _items.addAll(list);
    notifyListeners();
  }

  void clear(){
    _items.clear();
  }

  void removeAll(){
    _items.clear();
    notifyListeners();
  }

  void removeOne(NewsModelShuu item){
    _items.remove(item);
    notifyListeners();
  }

  void addOne(NewsModelShuu item){
    _items.add(item);
    notifyListeners();
  }

  void insert(NewsModelShuu item){
    _items.insert(0, item);
    notifyListeners();
  }
}