import 'package:cached_network_image/cached_network_image.dart';
import 'package:confirm_dialog/confirm_dialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_app_01/global.key.dart';
import 'package:test_app_01/model/news/models.dart';
import 'package:test_app_01/provider/index.dart';
import 'package:test_app_01/screen/webview.dart';
import 'package:test_app_01/utilities/globals.dart';
import 'package:intl/intl.dart';

class ListItemWidget extends StatefulWidget {
  final NewsModelShuu oneNews;

  const ListItemWidget({
    Key? key,
    required this.oneNews,
  }) : super(key: key);

  @override
  State<ListItemWidget> createState() => _ListItemWidgetState();
}

class _ListItemWidgetState extends State<ListItemWidget> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.oneNews.title),
      leading: widget.oneNews.imgUrl != null
          ? SizedBox(
              width: 100,
              child: AnimatedAlign(
                alignment: widget.oneNews.id ==
                        Provider.of<NewsProvider>(GlobalKeys.currentKey.currentState!.context, listen: false)
                            .animationId
                    ? Alignment.bottomLeft
                    : Alignment.bottomRight,
                duration: const Duration(seconds: 1),
                curve: Curves.fastOutSlowIn,
                child: SizedBox(
                  width: 50,
                  child: CachedNetworkImage(
                    imageUrl: Globals.domainHost + widget.oneNews.imgUrl!,
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.fitWidth,
                            colorFilter: const ColorFilter.mode(Colors.white, BlendMode.colorBurn)),
                      ),
                    ),
                    placeholder: (context, url) => const Center(child: CircularProgressIndicator()),
                    errorWidget: (context, url, error) => const Icon(Icons.error),
                  ),
                ),
              ),
            )
          : const Icon(Icons.image),
      trailing: Text(
        "${DateFormat('yyyy-MM-dd').format(widget.oneNews.createdDate)}\n${DateFormat('HH:mm:ss').format(widget.oneNews.createdDate)}",
      ),
      onTap: () async {
        Provider.of<NewsProvider>(GlobalKeys.currentKey.currentState!.context, listen: false).animationId =
            widget.oneNews.id;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => WebViewScreen(
                      title: 'Мэдээ',
                      id: widget.oneNews.id,
                    )));
        // widget.animationId = widget.news.id;
        // Get.to(() => WebViewScreen(
        //   id: widget.news.id,
        //   title: 'Мэдээ',
        // ))!
        //     .then((value) {
        //   setState(() {
        //     if (widget.isAnimation == false)
        //       widget.isAnimation = true;
        //     else
        //       widget.isAnimation = false;
        //   });
        // });
      },
      onLongPress: () async {
        if (await confirm(
          context,
          title: const Text('Устгах'),
          content: Text('Та ${widget.oneNews.title} гэсэн мэдээг утгах уу?'),
          textOK: const Text('Тийм'),
          textCancel: const Text('Үгүй'),
        )) {
          debugPrint("OK shuu");
          if (mounted){
            Provider.of<NewsProvider>(GlobalKeys.currentKey.currentState!.context, listen: false)
                .removeOne(widget.oneNews);
          }
        }
      },
    );
  }
}
