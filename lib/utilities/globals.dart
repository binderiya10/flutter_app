class Globals {
  static String domainHost = "https://trade.mn";
  static String newsHost = "https://trade.mn/news/";
  static String domain = "trade.mn";
  static int port = 80;
  static String apiNews = "/api/v1/news";
  static int pageSize = 10;
}