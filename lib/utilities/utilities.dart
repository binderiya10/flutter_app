import 'package:flutter/material.dart';

class Utilities {
  static void loadingShow(BuildContext context) {
    showDialog(
        // The user CANNOT close this dialog  by pressing outsite it
        barrierDismissible: false,
        // barrierColor: Colors.white.withOpacity(0.1),
        // barrierColor: Colors.transparent,
        context: context,
        builder: (_) {
          return Dialog(
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,

            // backgroundColor: Colors.white.withOpacity(0.1),
            // shadowColor: Colors.white.withOpacity(0.1),

            child: const Center(child: CircularProgressIndicator()),
          );
        });
  }
}
