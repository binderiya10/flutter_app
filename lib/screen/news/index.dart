/**
 * Мэдээний жагсаалтын хуудас
 */
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:test_app_01/bloc/news/index.bloc.dart';
import 'package:test_app_01/bloc/news/index.event.dart';
import 'package:test_app_01/bloc/news/index.state.dart';

import 'package:test_app_01/model/news/models.dart';
import 'package:test_app_01/provider/index.dart';
import 'package:test_app_01/screen/news/newsAdd/index.dart';

import 'package:test_app_01/utilities/utilities.dart';
import 'package:test_app_01/widget/common/main_app_bar.dart';
import 'package:test_app_01/widget/common/main_button.dart';
import 'package:test_app_01/widget/news/list_item_widget.dart';

part 'index.widgets.dart';

part 'index.functions.dart';

part 'index.listeners.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({Key? key}) : super(key: key);

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  final _bloc = NewsBloc();

  // void throwTest(){
  //   try {
  //     try {
  //       throw 1;
  //     } catch (e, s) {
  //       print("error1 $e $s");
  //       rethrow;
  //     }
  //   } catch (e2, s2) {
  //     print("error2 $e2 $s2");
  //   }
  // }

  // final List<NewsModelShuu> _list = [];
  Map<String, String> filterList = {"": "Бүгд", "NEWS_CATEGORY_01": "Биржийн мэдээ", "NEWS_CATEGORY_02": "Мэдээлэл"};
  final RefreshController _refreshController = RefreshController();
  final ScrollController _scrollController = ScrollController();

  bool isAnimation = false;
  bool loading = true;

  String animationId = '';
  String? selectedFilter = "";

  int pageId = 0, totalPages = 0;

  @override
  void initState() {
    initValue();
    super.initState();
    // throwTest();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MainAppBar(
        title: "Мэдээ",
      ),
      body: BlocListener(
        bloc: _bloc,
        listener: _listener,
        child: _mainWidgets,
      ),
    );
  }

  @override
  void dispose() {
    _bloc.close();
    _refreshController.dispose();
    super.dispose();
  }
}
