import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:test_app_01/model/news/models.dart';
import 'package:test_app_01/provider/index.dart';
import 'package:test_app_01/widget/common/main_app_bar.dart';
import 'package:test_app_01/widget/common/main_button.dart';
import 'package:test_app_01/widget/common/main_input.dart';
part 'index.functions.dart';
part 'index.widgets.dart';

class NewsAddPage extends StatefulWidget {
  const NewsAddPage({Key? key}) : super(key: key);

  @override
  State<NewsAddPage> createState() => _NewsAddPageState();
}

class _NewsAddPageState extends State<NewsAddPage> {
  final TextEditingController _titleController = TextEditingController();

  _imagePicker() async {
    debugPrint("OK");
    final ImagePicker picker = ImagePicker();
// Pick an image.
    final XFile? image = await picker.pickImage(source: ImageSource.gallery);
    // final LostDataResponse response = await picker.retrieveLostData();
    // if (response.isEmpty) {
    //   return;
    // }
    // final List<XFile>? files = response.files;
    // if (files != null) {
    //   debugPrint(files.toString());
    // } else {
    //   Get.snackbar("Алдаа", response.exception!.message ?? "");
    // }
  }

  _save() {
    if (_titleController.text.isNotEmpty) {
      Provider.of<NewsProvider>(context, listen: false).insert(
        NewsModelShuu(
          id: Random().toString(),
          title: _titleController.text,
          imgUrl: "/uploads/id/news/kLpeFaJpMdybqJWhZUrV.png",
          createdDate: DateTime.now(),
        ),
      );
      Get.snackbar("Амжилттай", "Амжилттай");
      Navigator.pop(context);
    } else {
      Get.snackbar("Анхаар", "Гарчиг оруулна уу");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MainAppBar(
        title: "Мэдээ нэмэх",
      ),
      body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(
              top: 30,
              bottom: 30,
              left: 15,
              right: 15,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                MainInput(
                  controller: _titleController,
                  hintText: "Гарчиг оруулна уу",
                ),
                const SizedBox(
                  height: 10,
                ),
                GestureDetector(
                  onTap: () => _imagePicker,
                  child: Container(
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey,
                          width: 1,
                        )),
                    child: const Text("Зураг оруулах"),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Mainbutton(
                  text: 'Хадгалах',
                  onPress: _save,
                ),
                const SizedBox(
                  height: 30,
                )
              ],
            ),
          )),
    );
  }
}
