part of 'index.dart';

extension _Listeners on _NewsPageState {
  void _listener(context, state) {
    if (state is NewsLoading) {
      // Utilities.loadingShow(context);
      loading = true;
      setState(() {

      });
    } else if (state is NewsFailure) {
      // Navigator.of(context).pop();
      Get.snackbar('Алдаа', "${state.statusCode ?? ""}: ${state.message ?? ""}");
      loading = false;
      setState(() {

      });
    } else if (state is NewsSuccessful) {
      _refreshController.loadComplete();
      _refreshController.refreshCompleted();
      // Navigator.of(context).pop();
      debugPrint("success shuu");
      debugPrint("lenght ${state.news.length}");
      totalPages = state.totalPages;
      if (pageId == 0) {
        Provider.of<NewsProvider>(context, listen: false).clear();
      }
      Provider.of<NewsProvider>(context, listen: false).items = state.news;
      loading = false;
      setState(() {

      });
      // setState(() {
      //   _list.addAll(state.news);
      // });
    }
  }
}
