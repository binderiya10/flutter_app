/**
 * Мэдээний жагсаалтын функцууд
 */
part of 'index.dart';

extension _Functions on _NewsPageState {

  /// Мэдээний жагсаалт авчирна. Цонх анх дуудахад ажиллана.
  void initValue() {
    _bloc.add(const GetNews(pageId: 0, filter: ''));
    _scrollController.addListener(() {
      if (_scrollController.position.atEdge) {
        debugPrint("scroll position ${_scrollController.position.pixels}");
        bool isTop = _scrollController.position.pixels == 0;
        if (isTop) {
          debugPrint('At the top');
          _onRefresh();
        } else {
          debugPrint('At the bottom');
          _onLoading();
        }
      }
    });
  }

  /// Мэдээний жагсаалт шүүлтүүр хийнэ
  _onFilter(value) async {
    pageId = 0;


    // ignore: invalid_use_of_protected_member
    setState(() {
      selectedFilter = value;
    });
    _bloc.add(GetNews(pageId: pageId, filter: selectedFilter ?? ''));
    _refreshController.position!.animateTo(0, duration: const Duration(milliseconds: 500), curve: Curves.elasticOut);

    // _refreshController.resetNoData();
    // await getNews(page, _selectedFilter ?? "");
  }


  _onLoading() {
    if (totalPages > pageId) {
      pageId++;
      _bloc.add(GetNews(filter: selectedFilter ?? '', pageId: pageId));
    } else {
      _refreshController.loadNoData();
    }
  }

  Future<void> _onRefresh() async {
    pageId = 0;
    _bloc.add(GetNews(filter: selectedFilter ?? '', pageId: pageId));
  }
}
