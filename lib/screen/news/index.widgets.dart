part of 'index.dart';

extension _Widgets on _NewsPageState {
  Widget get _mainWidgets {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
              Mainbutton(
                text: "Нэмэх",
                onPress: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => const NewsAddPage()));
                },
              ),
              Align(
                alignment: Alignment.topRight,
                child: DropdownButton(
                  items: filterList.entries.map((e) => DropdownMenuItem(value: e.key, child: Text(e.value))).toList(),
                  value: selectedFilter,
                  onChanged: (val) => _onFilter(val),
                ),
              ),
              IntrinsicHeight(
                child: const Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 20),
                  child: Center(child: Text("Тайлбар: Та устгах бол мэдээн дээр удаан дарна уу.")),
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            controller: _refreshController,
            onLoading: _onLoading,
            onRefresh: _onRefresh,
            child: Consumer<NewsProvider>(
              builder: (context, provider, child) {
                return ListView.builder(
                  itemCount: provider.items.length,
                  itemBuilder: (_, index) => ListItemWidget( oneNews: provider.items[index]),
                  controller: _scrollController,
                );
              },
            ),
            // child: ListView(
            //   children: drawNews(_list),
            // ),
          ),
        ),
        (loading == true) ? Container(padding: EdgeInsets.all(20), child: Text("Ачааллаж байна ..."),) : Container()
      ],
    );
  }

  List<Widget> drawNews(List<NewsModelShuu> list) {
    List<Widget> ret = [];

    for (var news in list) {
      ret.add(
        ListItemWidget(
          oneNews: news,
        ),
      );
      ret.add(const SizedBox(
        height: 10,
      ));
    }
    return ret;
  }
}
