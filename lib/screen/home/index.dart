import 'package:flutter/material.dart';
import 'package:test_app_01/widget/common/main_app_bar.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MainAppBar(
        title: "Нүүр хуудас",
      ),
      body: Container(),
    );
  }
}
