part of models;

@JsonSerializable()
class NewsModelShuu {
  String id;
  String title;
  String? imgUrl;
  DateTime createdDate;

  NewsModelShuu({
    required this.id,
    required this.title,
    this.imgUrl,
    required this.createdDate,
  });

  factory NewsModelShuu.fromJson(Map<String, dynamic> json) => _$NewsModel2FromJson(json);

  Map<String, dynamic> toJson() => _$NewsModel2ToJson(this);
}