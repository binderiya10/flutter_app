// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      json['name'] as String,
      json['email'] as String,
    );

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
    };

NewsModelShuu _$NewsModel2FromJson(Map<String, dynamic> json) => NewsModelShuu(
      id: json['id'] as String,
      title: json['title'] as String,
      imgUrl: json['image_path'] as String?,
      createdDate: DateTime.parse(json['created_at'] as String),
    );

Map<String, dynamic> _$NewsModel2ToJson(NewsModelShuu instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'imgUrl': instance.imgUrl,
      'createdDate': instance.createdDate.toIso8601String(),
    };


