library models;

import 'package:json_annotation/json_annotation.dart';

part 'news.dart';
part 'news.detail.dart';
part 'dto/store.dart';

part 'NewsModel.dart';
part 'NewsModel2.dart';
part 'models.g.dart';